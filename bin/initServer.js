const { exec, spawn } = require("child_process");
const { normalize } = require("path");

const { get } = require("http");

const { password } = require(__dirname + "/../server/src/ormconfig");

exec(
  `mysql -u root -p'${password}' < ${__dirname}/../src/clearDb.sql`,
  (error, stdout, stderr) => {
    if (error) {
      console.error("[Error:");
      console.error(error);
      console.error("]");
      return;
    }
    console.log(stdout);
  }
);

const server = spawn("npm", ["run", "dev"], {
  cwd: `${normalize(`${__dirname}/../server`)}`,
});
let selenium;

server.on("data", (data) => {
  console.log(data);
});

server.stdout.setEncoding("utf8");
server.stdout.on("data", (data) => {
  console.log("stdout: " + data);
  if (data.match("Express server has started on port 3030")) {
    get("http://localhost:3030/city/generate", (resp) => {
      let data = "";

      resp.on("data", (chunk) => {
        data += chunk;
      });

      resp.on("end", () => {
        console.log(JSON.parse(data));

        setTimeout(() => {
          console.log(normalize(`${__dirname}/../tests/functional`))
          selenium = spawn("selenium-side-runner", ["dbTest.side"], {
            cwd: normalize(`${__dirname}/../tests/functional`),
          });
          setTimeout(() => {
            console.log(`mysql -u root -p'${password}' kuSobie < ${__dirname}/../src/initDb.sql`)
            exec(
                `mysql -u root -p'${password}' kuSobie < ${__dirname}/../src/initDb.sql`,
                (error, stdout, stderr) => {
                  if (error) {
                    console.error("[Error:");
                    console.error(error);
                    console.error("]");
                    return;
                  }
                  console.log(stdout);
                }
              );            
          }, 10000);
          selenium.stdout.setEncoding("utf8");
          selenium.stdout.on("data", (data) => {
            console.log(data);
            console.log(data.match("closing code: 0"));
            if (data.match("closing code: 0")) {
              console.log("exec mysql");
            }
          });
          selenium.stderr.setEncoding("utf8");
          selenium.stderr.on("data", (data) => {
            console.log("stderr: " + data);
          });

          selenium.on("close", (code) => {
            console.log("closing code: " + code);
          });
        }, 50000);
      });
    }).on("error", (err) => {
      console.log("Error: " + err.message);
    });
  }
});

server.stderr.setEncoding("utf8");
server.stderr.on("data", (data) => {
  console.log("stderr: " + data);
});

server.on("close", (code) => {
  console.log("closing code: " + code);
});
