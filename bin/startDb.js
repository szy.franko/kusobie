const { exec, spawn } = require("child_process");
const { normalize } = require("path");
const { password } = require(normalize(__dirname + "\\..\\server\\src\\ormconfig"));

exec(
    `c:\\xampp\\mysql\\bin\\mysql.exe -u root -p${password} kusobe < ${__dirname}\\..\\src\\startDb.sql`,
    (error, stdout, stderr) => {
      if (error) {
        console.error("[Error:");
        console.error(error);
        console.error("]");
        return;
      }
      console.log(stdout);
    }
  );