import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.kusobie.app',
  appName: 'mobile',
  webDir: 'www'
};

export default config;
