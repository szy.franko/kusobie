const years = (y: number | undefined): string => {
  if (y == undefined) return '';
  if (y <= 19) {
    return 'lat'
  }
  const stringYears = y + '';
  if (stringYears.at(-1) === '2' || stringYears.at(-1) === '3' ||
    stringYears.at(-1) === '4'
  ) {
    return 'lata';
  } else return 'lat';
}

export default years;
