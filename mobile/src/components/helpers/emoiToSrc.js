const emoiToSrc = {
  '_)$': 'smile.png',
  '3)$': 'laugh.png',
  ':D': 'grin.png',
  '8D': 'cool.png',
  '8E': 'sunglasses.png',
  ':handwave:': 'wave.png',
};

export default emoiToSrc;
