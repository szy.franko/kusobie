import mitt from 'mitt';
type UnreceivedMessage = {
  content: string,
  datetime: string
}


interface UnreceivedMessages {
  [key: string]: UnreceivedMessage[];
}



// Definiujemy typy dla eventów dynamicznych i stałych
interface DynamicEvents {
  [key: string]: { content: string; datetime: string };
}

interface StaticEvents {
  'clear-input'?: void;
  'scroll-to-bottom'?: void;
  'scrolled-to-bottom'?: void;
  'scrolled-to-top'?: void;
  'unreceivedMessages'?: UnreceivedMessages,
  'blocked': string;
  'unblocked': string;
  'loggedIn': string;
  'loggedOut': string;
  // 'unreceivedMessages': void
}

// Tworzymy typ union, który łączy oba powyższe interfejsy
type Events = {
  [K in keyof StaticEvents]: StaticEvents[K];
} & {
  [key: string]: DynamicEvents[keyof DynamicEvents];
  [key: symbol]: unknown; // Ensure that the Events type can accept symbols as keys
};

// Inicjalizujemy mitt z typem Events
const eventBus = mitt<Events>();

export default eventBus;
