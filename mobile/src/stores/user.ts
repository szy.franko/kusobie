// src/stores/exampleStore.js
import { defineStore } from 'pinia'


import User from '../interfaces/User';
export const useUserStore = defineStore('user', {
  state: () => ({
    user: {
      about: '',
      age: 0,
      ageRange: null,
      city_id: 0,
      distance: 0,
      email: '',
      height: 0,
      id: 0,
      loggedIn: false,
      nick: '',
      photos: {
        profile: '',
        rest: []
      },
      sex: false,
      state: '',
      uuid: '' // Dodaj domyślną wartość dla uuid
    } as User
  }),
  actions: {
    increment() {
      // this.counter++
    },
    setUser(user: User) {
      this.user = user;
    },
    wanishUser() {
      this.user = {
        about: '',
        age: 0,
        ageRange: null,
        city_id: 0,
        distance: 0,
        email: '',
        height: 0,
        id: 0,
        loggedIn: false,
        nick: '',
        photos: {
          profile: '',
          rest: []
        },
        sex: false,
        state: '',
        uuid: '' // Dodaj domyślną wartość dla uuid
      }
    },
    setUuid(uuid: string) {
      this.user.uuid = uuid;
    },
    setProfilePhoto(profile: string) {
      debugger
      this.user.photos.profile = profile;
    },
    setRestPhoto(photo: string, number: number) {
      this.user.photos.rest[number] = photo;
    },
    removeRestPhoto(number: number) {
      this.user.photos.rest.splice(number, 1)
    }
  }
})
