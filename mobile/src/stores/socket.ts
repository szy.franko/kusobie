import { defineStore } from 'pinia';
import eventBus from '../eventBus';
import { useUserStore } from './user';

import Message from 'src/interfaces/Message';
import UnreceivedMessages from 'src/interfaces/UnreceivedMessages';
import UnreadMessage from 'src/interfaces/UnreadMessage';



export const useSocketStore = defineStore('socket', {
  state: () => ({
    ws: null as WebSocket | null,
    unreceivedMessages: {} as UnreceivedMessages,
    unreadMessages: [] as UnreadMessage[],
    selectedPerson: '' as string
  }),
  actions: {
    setSocket(url: string) {
      const userStore = useUserStore();
      const { user } = userStore;
      this.ws = new WebSocket(url);

      this.ws.onmessage = ({ data }) => {
        const parsedData = JSON.parse(data);
        if (parsedData.message) {
          const newMessage: Message = {
            content: parsedData.message.content,
            datetime: parsedData.message.datetime,
          };

          if (this.unreceivedMessages[parsedData.message.nick]) {
            this.unreceivedMessages[parsedData.message.nick].push(newMessage);
          } else {
            this.unreceivedMessages[parsedData.message.nick] = [newMessage];
          }

          eventBus.emit('unreceivedMessages', this.unreceivedMessages);
          eventBus.emit(parsedData.message.nick, newMessage);
        }

        if ('blocked' in parsedData) {
          eventBus.emit('blocked', parsedData.blocked.nick);
        }
        if ('unblocked' in parsedData) {
          eventBus.emit('unblocked', parsedData.unblocked.nick);
        }

        if (parsedData.loggedIn) {
          eventBus.emit('loggedIn', parsedData.loggedIn);
        }
        if (parsedData.loggedOut) {
          eventBus.emit('loggedOut', parsedData.loggedOut);
        }
      };

      if (this.ws) {
        this.ws.addEventListener('open', () => {
          if (this.ws) {
            this.ws.send(JSON.stringify({ uuid: user.uuid }));
          }
        });
      }

      this.ws.addEventListener('error', (error) => {
        console.error('WebSocket error observed:', error);
      });
    },

    removeUserFromUnreadMessages(nick: string) { // Fixed typo here
      this.unreadMessages = this.unreadMessages.filter(user => user.nick !== nick);
    },
    selectPerson(person: string) {
      this.selectedPerson = person;
      this.unreadMessages = this.unreadMessages.filter(user => user.nick !== person);
      for (const nick in this.unreceivedMessages) {
        if (nick === person) {
          delete this.unreceivedMessages[nick]
        }
      }
    }
    ,
    setUnreadMeassages(messages: { nick: string, number: number }[]) {
      this.unreadMessages = messages;

    }
  },
});
