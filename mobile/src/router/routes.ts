import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: 'stories', component: () => import('pages/StoriesPage.vue') },
      { path: 'about', component: () => import('pages/AboutPage.vue') },
      {
        path: 'search', component: () => import('pages/SearchPage.vue')
      },
      { path: 'messages', component: () => import('pages/Messages.vue') },
      { path: 'personDetails', component: () => import('pages/PersonDetails.vue') },
      { path: 'communicatorPage', component: () => import('pages/CommunicatorPage.vue') },
      { path: 'matchingPage', component: () => import('pages/MatchingPage.vue') },
      { path: 'visitsPage', component: () => import('pages/VisitsPage.vue') },
      { path: 'profilePage', component: () => import('pages/ProfilePage.vue') },


    ],
  },
  {
    path: '/config',
    component: () => import('layouts/ConfigLayout.vue'),
    children: [
      { path: '', component: () => import('pages/ConfigPage.vue') },
    ],
  },
  // {
  //   path: '/profile',
  //   component: () => import('layouts/ProfileLayout.vue'),
  //   children: [
  //     { path: '', component: () => import('../pages/Search.Page.vue') },
  //   ],
  // },
  // Always leave this as last one, but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
