export default interface City {
  id: number;
  name: string;
  type: string;
  latitude: string;
  longitude: string;
  commune: string;
  district: string;
  province: string;

}
