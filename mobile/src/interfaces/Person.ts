export default interface Person {
  nick: string;
  nrOfMessages?: number;
  loggedIn?: boolean;
  blocks?: boolean;
  blocked?: boolean;
}
