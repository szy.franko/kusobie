export default interface User {
  about: string;
  age: number;
  ageRange: number[] | null;
  city_id: number;
  distance: number;
  email: string;
  height: number;
  id: number;
  loggedIn: boolean;
  nick: string;
  photos: {
    profile: string;
    rest: string[];
  };
  sex: boolean;
  state: string;
  uuid?: string;
}
