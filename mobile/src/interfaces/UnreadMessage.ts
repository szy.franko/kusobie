export default interface UnreadMessage {
  nick: string;
  number?: number;
}
