import Message from './Message';
export default interface UnreceivedMessages {
  [key: string]: Message[];
}
