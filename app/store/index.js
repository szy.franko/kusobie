import { off } from "process";

export const state = () => ({
  user: {
  },
  ws: null,
  unreceivedMesages: {},
  $bus: null,
  selectedPerson: null,
  unreadMeassages: [],
})

export const getters = {

  getEmail(state) {
    return state.user.email;
  },

  getImageName(state) {
    return state.user.imageName;
  },

  getUser(state) {
    return state.user;
  },

  unreceivedMesages(state){
    return state.unreceivedMesages;
  }
}

export const mutations = {

  setEmail(state, email) {
    state.user.email = email
  },

  addImageName(state, image) {
    if (!state.user.images) {
      state.user.images = [];
    }
    state.user.images.push(image)
  },
  changeProfilePhoto(state, image) {
    state.user.photos.profile=image;
  },

  addRestImage(state, image) {
    state.user.photos.rest.push(image);
  },

  replaceRestImage(state, data) {
    state.user.photos.rest[data.index] = data.image;
  },

  removeRestImage(state, index) {
    state.user.photos.rest.splice(index, 1);
  },

  setUuid(state, uuid) {
    state.user.uuid = uuid;
  },

  setUser(state, user) {
    state.user = { ...state.user, ...user };
  },

  setSocket(state, url) {
    state.ws = new WebSocket(url);

    state.ws.onmessage = ({data})=>{
      const parsedData = JSON.parse(data);

      if(Object.keys(parsedData).includes('message')) {
        const newMessage = { content: parsedData.message.content, datetime: parsedData.message.datetime }

        if(state.unreceivedMesages[parsedData.message.nick]) {
          state.unreceivedMesages[parsedData.message.nick].push(newMessage);
        } else {
          state.unreceivedMesages[parsedData.message.nick] = [newMessage];
        };

        state.$bus.$emit('unreceivedMesages',state.unreceivedMesages);
        state.$bus.$emit(parsedData.message.nick, newMessage);

      }

      if(Object.keys(parsedData).includes('loggedOut')) {
        state.$bus.$emit('loggedOut', parsedData.loggedOut);
      }

      if(Object.keys(parsedData).includes('loggedIn')) {
        state.$bus.$emit('loggedIn', parsedData.loggedIn);
      }

      if(Object.keys(parsedData).includes('blocked')) {
        debugger
        state.$bus.$emit('blocked', parsedData.blocked.nick);
      }

      if(Object.keys(parsedData).includes('unblocked')) {

        state.$bus.$emit('unblocked', parsedData.unblocked.nick);
      }

      if(Object.keys(parsedData).includes('loggedOut')){
        state.$bus.$emit('loggedOut', parsedData.loggedOut);
      }
      if(Object.keys(parsedData).includes('loggedIn')){
        state.$bus.$emit('loggedIn', parsedData.loggedIn);
      }
    }

    state.ws.addEventListener('open', (event) => {
      state.ws.send(JSON.stringify({uuid: state.user.uuid}));
    });
  },

  setBus(state,$bus) {
    state.$bus = $bus;
  },

  wanishUser(state) {
    state.user = {};
    state.unreadMeassages = [];
    state.unreceivedMesages = []
  },

  selectPerson(state, person) {
    state.selectedPerson = person;
  },

  changeAgeRange(state, ageRange) {
    state.user.ageRange = ageRange;
  },
  changeDistance(state, distance) {
    state.user.distance = distance;
  },
  setUnreadMeassages(state, unreadMeassages){
    state.unreadMeassages = unreadMeassages;
  },
  removeUserFromUnreadMeassages(state, nick){
    state.unreadMeassages = state.unreadMeassages.filter(user => user.nick !== nick);
  }

}

export const actions = {

  setEmail(context, email) {
    context.commit('setEmail', email);
    return email;
  },

  addImageName(context, image) {
    context.commit('addImageName', image);
  },

  changeProfilePhoto(context, image) {
    context.commit('changeProfilePhoto', image);
  },

  addRestImage(context, image) {
    context.commit('addRestImage', image);
  },

  removeRestImage(context, index) {
    context.commit('removeRestImage', index);
  },

  replaceRestImage(context, data) {
    context.commit('replaceRestImage', data);
  },

  setUuid(context, uuid) {
    context.commit('setUuid', uuid);
  },

  setUser(context, user) {
    context.commit('setUser', user);
  },

  setSocket(context, url) {
    context.commit('setSocket', url)
  },

  setBus(context, $bus) {
    context.commit('setBus', $bus);
  },

  wanishUser(context) {
    context.commit('wanishUser');
  },

  selectPerson(context  , person) {
    context.commit('selectPerson', person)
  },

  changeAgeRange(context, ageRange) {
    context.commit('changeAgeRange', ageRange);
  },

  changeDistance(context, distance) {
    context.commit('changeDistance', distance);
  },
  setUnreadMeassages(context, unreadMeassages){
    context.commit('setUnreadMeassages', unreadMeassages);
  },

  removeUserFromUnreadMeassages(context, nick){
    context.commit('removeUserFromUnreadMeassages', nick);
  }

}
