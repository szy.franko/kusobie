import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  // env: {
  //   serverUrl: `${process.env.VUE_HOST}:${process.env.PORT}` || 'http://localhost:3030'
  // },

  publicRuntimeConfig: {
    serverUrl: `${process.env.APP_HOST}:${process.env.APP_PORT}`,
    webSocketUrl: `ws://${process.env.WS_HOST}:${process.env.APP_PORT}`
  },
  head: {
    titleTemplate: '%s - kusobie',
    title: 'kusobie',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Wix+Madefor+Display&display=swap'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '~/plugins/vue-file-agent.js', mode: 'client'},
    {src: '~/plugins/vuejs-cropper.js', mode: 'client'},
    {src: '~/plugins/vue-upload-multiple-image.js', mode: 'client'},
    '~/plugins/bus.client.js'
    
  ],


  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/dotenv',
    '@vueuse/nuxt',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
   // '@pinia/nuxt',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/'
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: '#ffffff',
          accent: '#FAED6A',
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: '#326775',
          accent: '#92c88a',
          secondary: '#d7acfc', 
          info: '#fb8372',
          warning: '#d9c894',
          error: '#fb8372', 
          success: '#4b8046' 
        },
        anchor: '#92c88a'
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
