import Vue from 'vue';
import VueLazyload from 'vue-lazyload'
import VueUploadMultipleImage from 'vue-upload-multiple-image';

Vue.use(VueLazyload);
Vue.use(VueUploadMultipleImage);
Vue.component('VueUploadMultipleImage',VueUploadMultipleImage);
