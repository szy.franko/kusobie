export default {
    beforeMount() {
        if (this.$store.state.user.uuid === undefined &&
            !localStorage.getItem('user')) {
            this.$router.push({ path: "/" });
        }
    },
}