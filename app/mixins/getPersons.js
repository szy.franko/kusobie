export default {
    mounted() {
        this.getPersons('all');
    },
    methods: {
        getPersons(mode) {
            let url;

            if(mode === 'nearby') {
                url = '/user/close-friends'
            }
            if(mode === 'all') {
                url = '/user/close-friends-sorted'
            }

            this.$axios.get(this.$config.serverUrl+url,{
                params: { uuid: this.$store.state.user.uuid}            
            }).then(({data})=>{
                this.persons = data.map(person=>{
                    return {
                        nick: person.nick,
                        age: person.age,
                        image: JSON.parse(person.photos).profile,
                        lazyImage: JSON.parse(person.photos).profile,
                        city: person.name,
                        loggedIn: person.loggedIn
                    }
            });
            this.$forceUpdate();
            }).catch(err=>{
                console.error(err);
            })
        }
    }
}