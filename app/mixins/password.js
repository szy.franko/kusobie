import { PasswordMeter } from "password-meter";
const metter = new PasswordMeter();

export default {
  data() {
    return {
      password: "",
      repeatPassword: "",
      metterColor: "light-red darken-4",
      metterValue: 0,
    };
  },
  watch: {
    password(newValue, oldValue) {
      const result = metter.getResult(newValue);
      this.metterValue = result.percent;
      console.log(result);
      switch (result.status) {
        case "veryWeak":
          this.metterColor = "red darken-4";
          this.passwordComplexity = false;
          break;
        case "weak":
          this.metterColor = "yellow darken-4";
          this.passwordComplexity = false;
          break;
        case "medium":
          this.metterColor = "orange darken-4";
          this.passwordComplexity = false;
          break;
        case "strong":
          this.metterColor = "light-green darken-4";
          this.passwordComplexity = true;
          break;
        case "perfect":
          this.metterColor = "light-green darken-4";
          this.passwordComplexity = true;
          break;
      }
      this.passwordsEqual = this.password === this.repeatPassword;
    },
    repeatPassword(newValue, oldValue) {
      this.passwordsEqual = this.password === this.repeatPassword;
    },
  },
};
