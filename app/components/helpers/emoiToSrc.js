const emoiToSrc = {
    '_\\)$': '_).png',
    '3\\)$': '3).png',
    ':D': ':D.png',
    '8D': '8D.png',
    '8E': '8E.png',
    ':handwave:': 'handwave.svg'
}
export default emoiToSrc;