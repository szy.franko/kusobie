<template>
  <div :class="{ hidden: !visible }">
    <v-bottom-navigation
      v-if="isLargeScreen"
      ref="large-menu"
      class="menu upper"
      :value="value"
      color="primary"
      grow
    >
      <v-tooltip v-if="$store.state.user.uuid" bottom>
        <template v-slot:activator="{ on, attrs }">
          <span style="position: relative; top: 12px" v-bind="attrs" v-on="on">
            <v-list-item-avatar @click="goToProfileDetails">
              <v-img
                :class="{ visible: imageVisible }"
                :src="
                  $store.state.user.photos?.profile ||
                  ($store.state.user.nick &&
                    $config.serverUrl + '/images/profile.jpg')
                "
              ></v-img>
            </v-list-item-avatar>
          </span>
        </template>
        <span>Ustawienia profilu</span>
      </v-tooltip>
      <v-btn
        v-for="(item, index) in items"
        :key="index"
        @click="
          () => {
            handleClick(index);
          }
        "
      >
        <span
          >{{ item.title }}
          <v-badge
            v-if="item.title === 'Wiadomości' && nrOfMessages > 0"
            color="info"
            :content="nrOfMessages"
          >
          </v-badge>
        </span>
        <v-icon>{{ item.icon }}</v-icon>
      </v-btn>
    </v-bottom-navigation>
    <v-card v-if="!isLargeScreen" class="menu-mobile">
      <v-navigation-drawer v-model="drawer" :mini-variant.sync="mini" permanent>
        <v-list-item v-if="$store.state.user.uuid" class="px-2">
          <v-tooltip bottom>
            <template v-slot:activator="{ on, attrs }">
              <span v-bind="attrs" v-on="on">
                <v-list-item-avatar @click="goToProfileDetails">
                  <v-img
                :class="{ visible: imageVisible }"
                :src="
                  $store.state.user.photos?.profile ||
                  ($store.state.user.nick &&
                    $config.serverUrl + '/images/profile.jpg')
                "
              ></v-img>
                </v-list-item-avatar>
              </span>
            </template>
            <span>Ustawienia profilu</span>
          </v-tooltip>
          <!-- <v-list-item-avatar @click="goToProfileDetails">
            <v-img :src="$store.state.user.photos?.profile"></v-img>
          </v-list-item-avatar> -->
          <v-list-item-title>{{ $store.state.user.nick }}</v-list-item-title>
          <v-btn icon @click.stop="mini = !mini">
            <v-icon>mdi-chevron-left</v-icon>
          </v-btn>
        </v-list-item>
        <v-divider></v-divider>
        <v-list dense>
          <v-list-item
            v-for="(item, index) in items"
            :key="index"
            @click="
              () => {
                handleClick(index);
              }
            "
          >
            <v-list-item-icon>
              <v-icon>{{ item.icon }}</v-icon>
            </v-list-item-icon>
            <v-list-item-content
              @click="
                () => {
                  handleClick(index);
                }
              "
            >
              <v-list-item-title>{{ item.title }}</v-list-item-title>
            </v-list-item-content>
          </v-list-item>
        </v-list>
      </v-navigation-drawer>
    </v-card>
  </div>
</template>
<script>
import { useMediaQuery } from "@vueuse/core";
import { mapState } from 'vuex';
const isLargeScreen = useMediaQuery('(min-width: 750px)');

export default {
  name: 'MenuVComponent',

  data() {
    return {
      value: 1,
      isLargeScreen,
      drawer: true,
      mini: true,
      visible: true,
      imageVisible: false,
      isBadbge: null,
      nrOfMessages: 0,
    };
  },

  computed: {
    imageName() {
      // TODO poprawić photos
      // return this.$store.state.user.photos!==undefined? this.$store.state.user.photos.profile:'fake'
    },

    items() {
      let items;
      if (this.$store.state.user.uuid) {
        items = [
          { title: 'Szukaj miłości', icon: 'mdi-magnify' },
          { title: 'Wiadomości', icon: 'mdi-comment-multiple-outline' },
          { title: 'Dopasowania', icon: 'mdi-heart-multiple' },
          { title: 'Odwiedziny', icon: 'mdi-heart' },
        ];
      } else {
        items = [
          { title: 'Strona główna', icon: 'mdi-heart-outline' },
          { title: 'Historie', icon: 'mdi-heart' },
          { title: 'Pisali o nas', icon: 'mdi-heart-multiple' },
        ];
      }
      return items;
    },
    ...mapState(['unreceivedMesages']),
  },

  beforeMount() {
    document.addEventListener('scroll', (e) => {
      if (window.scrollY > 100) {
        this.$refs['large-menu']?.$el.classList.remove('upper');
      } else {
        this.$refs['large-menu']?.$el.classList.add('upper');
      }
    });
    this.$bus.$on('hide-avatar', () => {
      this.imageVisible = false;
    });
    this.$bus.$on('show-avatar', () => {
      this.imageVisible = true;
    });
  },

  updated() {
    if (this.$route.query?.nick) {
      this.value = 1;

      if (this.$route.query.operation === 'wave') {
        this.$bus.$emit('wave-to', { nick: this.$route.query.nick });
      }

      if (this.$route.query.operation === 'message') {
        this.$bus.$emit('message', { nick: this.$route.query.nick });
      }

      this.$bus.$emit('menu-item', this.items[1]);
    } else {
    }
  },

  mounted() {
    this.$bus.$on('unreceivedMesages', (data) => {
      console.log(this.$store)
      this.nrOfMessages = 0;
      for(let i=0, max=this.$store.state.unreadMeassages.length;
        i < max; i++) {
        this.nrOfMessages += this.$store.state.unreadMeassages[i].number;
      }
      for (let el in data) {
        if (el === this.$store.state.selectedPerson) {
          continue;
        }
        this.nrOfMessages += data[el].length;
      }
      // this.$forceUpdate();
      console.log(data);
    });
    this.$bus.$on('hide-menu', () => {
      this.visible = false;
    });
    this.$bus.$on('show-menu', () => {
      this.visible = true;
    });
  },

  methods: {
    handleClick(index) {
      this.$store.dispatch('selectPerson', null);
      this.$route.query.search='false';
      this.$router.replace({ query: { menu: this.items[index].title} });

      if (!this.$store.state.user.uuid) {
        switch (index) {
          case 0:
            this.$router.push('/');
            break;
          case 1:
            this.$router.push('/stories');
            break;
          case 2:
            this.$router.push('/about');
            break;
        }
      } else {
        this.$bus.$emit('menu-item', this.items[index]);
      }
    },

    goToProfileDetails() {
      this.$router.push({ path: '/profile/details' });
    },
  },
  watch: {

  },
};
</script>
<style scoped lang="scss">
.hidden {
  display: none;
}

.menu {
  position: fixed;
  z-index: 999;
  height: 50px !important;
  transform: translateY(-13px) !important;
  z-index: 200;

  &.upper,
  &:hover {
    height: 76px !important;
    transform: translateY(12px) !important;
  }
}

.menu-mobile {
  position: absolute;
  top: 0px;
  left: 0px;
}
.v-sheet.v-app-bar.v-toolbar:not(.v-sheet--outlined) {
  z-index: 200;
}
</style>
