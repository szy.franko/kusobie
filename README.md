﻿
# Dating portal "Ku sobie"

  

First, create a `.env` file in the app folder with the following content:

  

APP_HOST=http://localhost  

APP_PORT=3030  

WS_HOST=localhost  

DB_TYPE=mysql  

DB_HOST=localhost  

DB_PORT=3306  

DB_USERNAME=[name of the database user]  

DB_PASSWORD=[password]  

DB_DATABASE=kuSobie  

Remember to create a database named 'kuSobie'.  
  

## Run development version

  
Execute the following command from the root of the project directory:
`cd app && npm run dev`
In the second terminal, run:
`cd server && npm run dev`
f the server is running, make a GET request to the address http://localhost:3030/city/generate to insert Polish city names from a JSON file into the database..

  

## Build

`npm run generate && npm run build`

and in two different consoles

`cd dist && http-server`

and

`cd server && npm run start`

We assume that the http-server package is installed; if not, install it by running:

`npm install -g http-server`

# Sample database

If you want to run portal with some configured users just import kusobie.sql file,
i.e. run in the root of the project
`
mysql -u root -p kusobie < kusobie.sql
`
Then you can access two accounts with the following creditentials: login: kamil, password: CJAmili((&
and login: bianca, password: CJAmili((&.
