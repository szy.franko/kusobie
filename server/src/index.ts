import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { AppDataSource } from "./data-source";
import { Routes } from "./routes";
import * as cors from "cors";
const sharp = require("sharp");

const multer = require("multer");
const upload = multer({ dest: "uploads/" });


const app = express();
AppDataSource.initialize()
  .then(async () => {
    // create express app
    
    app.use(express.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
    app.use(bodyParser.json());
    app.use(cors());
    app.use("/images", express.static("uploads"));

    app.use((error, req, res, next) => {
      console.log(error);
      next()
    });

    // register express routes from defined application routes
    Routes.forEach((route) => {
      (app as any)[route.method](
        route.route,
        (req: Request, res: Response, next: Function) => {
          console.log(route.action);
          const result = new (route.controller as any)()[route.action](
            req,
            res,
            next
          );
          if (result instanceof Promise) {
            result.then((result) =>
              result !== null && result !== undefined
                ? res.send(result)
                : undefined
            );
          } else if (result !== null && result !== undefined) {
            res.json(result);
          }
        }
      );
    });

    // setup express app here+++++++++++++++++++++++++++++++++++++++++

    // non-standard routes

    app.post(
      "/files/upload",
      upload.single("file"),
      (req: Request, res: Response) => {
        console.log(req)
        sharp(`${__dirname}/../uploads/${req.file.filename}`)
          .resize({ width: 200 })
          .toFile(`${__dirname}/../uploads/${req.file.filename}.min`)
          .then(function (newFileInfo) {
            console.log("Success");
          })
          .catch(function (err) {
            console.error(err);
            console.log("Error occured");
          });

        res.json({ status: "OK", filename: req.file.filename });
      }
    );

    // start express server
    // app.listen(3030);

    // insert new users for test
    // await AppDataSource.manager.save(
    //     AppDataSource.manager.create(User, {
    //         nick: "Ja",
    //         email: "ja@a.pl",
    //         hash: "sadas324234",
    //         city: 'Łódź'
    //     })
    // )

    // await AppDataSource.manager.save(
    //     AppDataSource.manager.create(User, {
    //         nick:a "On",
    //         email: "on@a.pl",
    //         hash: "sa^&%^&das324234",
    //         city: 'Warszawa'
    //     })
    // )

    console.log(
      "Express server has started on port 3030. Open http://localhost:3030/user to see results"
    );
  })
  .catch((error) => console.log(error));

  export default app;
