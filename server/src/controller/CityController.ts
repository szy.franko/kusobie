import { NextFunction, Request, Response } from "express";
import * as fs from "fs";

import { AppDataSource } from "../data-source";
import { City } from "../entities/City.entity";
import Users from '../classes/Users';



export class CityController {
  private cityRepository = AppDataSource.getRepository(City);
  private cities: City[];

  private static R = 6371e3;
  private static users:object = Users.getUsers();

  constructor() {
    this.cities = JSON.parse(
      fs
        .readFileSync(
          __dirname + "/../../node_modules/polskie-miejscowosci/data.json"
        )
        .toString()
    );
  }

  async get(request: Request, response: Response, next: NextFunction) {
    const prefix: string = request.body.prefix?.toLowerCase();
    const selected: City[] | any = await this.cityRepository
      .createQueryBuilder("city")
      .where("city.name like :prefix", { prefix: `${prefix}%` })
      .getMany().catch(e=>{
          console.error(e);
      });
      console.log(selected);
    return { selected };
  }

  async getById(request: Request, response: Response) {
    const { id } = request.body;
    const city = await this.cityRepository.findOne({ where: { id } });
    if (!city) {
      return { status: 'NOT OK' };
    }
    console.log(city)
    return { ...city, status: 'OK' };
  }

  // generowanie miast, w produkcji !!!WYRZUCIĆ!!!
  async generate() {
    const file = fs
      .readFileSync(
        __dirname + "/../../node_modules/polskie-miejscowosci/data.json"
      )
      .toString();
    const cities = JSON.parse(file);
    let city;
    this.cities.forEach(async (el) => {
      let obj = {};
      for (let prop in el) {
        if (prop === "Id") {
          continue;
        }
        obj[prop.toLowerCase()] = el[prop];
      }
      city = AppDataSource.manager.create(City, obj);

      this.cityRepository.save(city);
    });
    return cities;
  }



  async getCloseCities(name, distance) {
    let cities = await this.cityRepository
      .createQueryBuilder('city')
      .getMany();
    
    const city = await this.cityRepository.findOne(
      { where: { name } }
    );
      
    cities = cities.filter(el => {
      CityController.distance(city.latitude,city.longitude, el.latitude, el.longitude)<=distance;
    });

  }



  static distance(lat1, lon1, lat2, lon2) {
    // const R = 6371e3; // metres
    const phi1 = lat1 * Math.PI / 180; // φ, λ in radians
    const phi2 = lat2 * Math.PI / 180;
    const deltaPhi = (lat2 - lat1) * Math.PI / 180;
    const deltaLambda = (lon2 - lon1) * Math.PI / 180;

    const a = Math.sin(deltaPhi / 2) * Math.sin(deltaPhi / 2) +
      Math.cos(phi1) * Math.cos(phi2) *
      Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return CityController.R * c; // in metres
  }
}
