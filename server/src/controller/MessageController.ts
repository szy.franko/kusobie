
import { AppDataSource } from '../data-source';
import { Request, Response } from 'express';
import { User } from '../entities/User.entity';
import { UserMessageUser } from '../entities/UserMessageUser.entity';
import { Message } from '../entities/Message.entity';
import { UserUser } from '../entities/UserUser.entity';

import Users from '../classes/Users';

export class MessageController {
    private userRepository = AppDataSource.getRepository(User);
    private userMessageUserRepository =
        AppDataSource.getRepository(UserMessageUser);
    private messageRepository = AppDataSource.getRepository(Message);
    private userUserRepository = AppDataSource.getRepository(UserUser);
    private saltRounds;
    private hash;

    private static users: object = Users.getUsers();

    constructor() {
        this.saltRounds = 10;
    }


    async wave(request: Request, response: Response) {
        const { uuid, nick } = request.body;

        const userFrom = await this.userRepository.findOne({
            where: { id: MessageController.users[uuid]?.id },
        });

        const userTo = await this.userRepository.findOne({ where: { nick } });
        if (!userFrom || !userTo) {
            return { status: 'NOT OK' };
        }

        const userUser = await this.userUserRepository
            .createQueryBuilder('userUser')
            .innerJoin('userUser.userFrom', 'userFrom')
            .innerJoin('userUser.userTo', 'userTo')
            .where(`userFromId=${userFrom.id} AND userToId=${userTo.id}`)
            .getMany();

        console.log('userUsers');
        console.log(userUser);
        if (userUser.length === 0) {
            console.log('users length 0')
            let newUser = { status: 'wave', userFrom, userTo };
            this.userUserRepository.save(newUser);
            return newUser;
            if (userUser[0].status === 'banned') {
                return { info: 'BANNED' };
            };

            if (userUser[0].status === 'friend') {
                return { info: 'SENDED' };
            };
        };


    }

    async getFriends(request: Request, response: Response) {
        const { uuid } = request.body;
        if (typeof MessageController.users[uuid] === 'undefined') {
            return {};
        }
        const { id } = MessageController.users[uuid];
        const userUsers = await AppDataSource.manager.query(
            `SELECT * FROM user_user 
            LEFT JOIN user ON user_user.userFromId=user.id
            LEFT JOIN user AS user0 ON user_user.userToId=user0.id
            WHERE user_user.userFromId=? OR user_user.userToId=?`,
            [id, id]
        );

        return userUsers;

    }
    async getMesssages(request: Request, response: Response) {
        const { uuid, friendNick } = request.query;
        if (typeof MessageController.users[uuid] === 'undefined') {
            return {};
        };
        const { id } = MessageController.users[uuid];
        const messagesUser = await AppDataSource.manager.query(
            `SELECT * FROM user_message_user 
        LEFT JOIN user ON user_message_user.userFromId=user.id
        LEFT JOIN user AS user0 ON user_message_user.userToId=user0.id
        LEFT JOIN message ON user_message_user.messageId = message.id
        WHERE (user.id=? AND user0.Nick=?) OR (user.nick=? AND user0.id=?)`,
            [id, friendNick, friendNick, id]
        );
        return messagesUser;
    }

    async messageRead(request: Request, response: Response) {
        const { uuid, nick } = request.body;
        console.log(uuid);
        console.log(nick);
        if(!MessageController.users[uuid]) {
            return;
        };
        const { id } = MessageController.users[uuid];
        const messagesUser = await AppDataSource.manager.query(
            `SELECT user_message_user.id, status,user.nick FROM user_message_user 
        LEFT JOIN user ON user_message_user.userToId=user.id
        LEFT JOIN user AS user0 ON user_message_user.userFromId=user0.id
        WHERE (user.id=? AND user0.Nick=? AND user_message_user.userToId=? AND user_message_user.userToId=user.id)`,
            [id, nick, id]
        );
        console.log(messagesUser);
        const messageUserChanged = await messagesUser.map(async el=> {
                await AppDataSource.manager.query(`
                UPDATE user_message_user SET status = 1 WHERE id = ?;
                `,[el.id]);    
            return el;
            
            // return AppDataSource.createQueryBuilder()
            // .update(UserMessageUser).set({status: 1})
            // .where('id = :id',{ id: el.id})
            // .execute();
        });

        return messageUserChanged;
        // return [];
    }
    async messageUnread(request: Request, response: Response) {
        const { uuid, nick } = request.body;
        console.log(uuid);
        console.log(nick);
        if(!MessageController.users[uuid]) {
            return;
        };
        const { id } = MessageController.users[uuid];
        const messagesUsers = AppDataSource.manager.query(`
        SELECT user.nick, COUNT(*) AS number FROM user 
        INNER JOIN user_message_user ON user_message_user.userFromId = user.id
        INNER JOIN user AS user0 ON user_message_user.userToId = user0.id
        WHERE user0.id = ? AND status = 0
        GROUP BY user.nick;
        `,[id]);
        return messagesUsers;
       
    }

}
