import * as fs from "fs";

import { AppDataSource } from '../data-source';
import { NextFunction, Request, Response } from 'express';
import { User } from '../entities/User.entity';
import { UserMessageUser } from '../entities/UserMessageUser.entity';
import { Message } from '../entities/Message.entity';
import { UserUser } from '../entities/UserUser.entity';
import { City } from '../entities/City.entity';
import Config from '../interfaces/Config';
import Users from '../classes/Users';
import { saveBase64ToFile } from '../helpers';

import { v4 as uuidv4 } from 'uuid';
import * as bcrypt from 'bcrypt';

import AppConfig from '../classes/AppConfig';

export class UserController {
  private userRepository = AppDataSource.getRepository(User);
  private userMessageUserRepository =
    AppDataSource.getRepository(UserMessageUser);
  private messageRepository = AppDataSource.getRepository(Message);
  private userUserRepository = AppDataSource.getRepository(UserUser);
  private cityRepository = AppDataSource.getRepository(City);
  private saltRounds;
  private hash;

  static users: object = Users.getUsers();
  static config: Config = AppConfig.getConfig();
  static uploadsPath: string = __dirname + '/../../uploads/'
  constructor() {
    this.saltRounds = 10;
  }

  async main(request: Request, response: Response) {
    return 'Kusobie service'
  }


  async checkEmail(request: Request, response: Response) {
    const { email } = request.query;
    const user = await this.userRepository.findOne({ where: { email } });
    if (user) {
      return { status: 'User exists' };
    }

    return { status: 'OK' };
  }


  async checkNick(request: Request, response: Response) {
    const { nick, uuid } = request.query;
    if (!UserController.users[uuid]) {
      return { status: 'NOT OK', message: 'User does not exist' };
    }
    const { id } = await UserController.users[uuid];
    const user = await this.userRepository.findOne({ where: { nick } });
    const userFrom = await this.userRepository.findOne({ where: { id } });
    const user_user = await this.userUserRepository.createQueryBuilder("userUser")
  .where("userUser.userFromId = :id AND userUser.userToId = :userId", { id, userId:user.id })
  .getMany(); 

    if (user_user.length > 0) {
      if (user_user[0].relation == null && user_user[0].handshake == null) {
        user_user[0].visited = true;
      }
    } else {
      const userUser = new UserUser();
      userUser.userFrom = userFrom;
      userUser.userTo = user;
      userUser.visited = true;
      this.userUserRepository.save(userUser);
    }


    if (user) {
      return { status: 'User exists' };
    }


    return { status: 'OK' };
  }

  async register(request: Request, response: Response, next: NextFunction) {
    const { nick, email, password, city, profileImage } = request.body;

    
    if(typeof profileImage!=='undefined') {
      const base64WithoutPrefix = profileImage.replace(/^data:[^;]+;base64,/, '');
      const binaryString = atob(base64WithoutPrefix);
      const fileSizeInBytes = binaryString.length;
      const fileSizeInKB = fileSizeInBytes / 1024;
      const fileSizeInMB = fileSizeInKB / 1024;
      if (fileSizeInMB > 9) return { status: 'NOT OK', message: 'Profile image too large' };
    }
 

    if (
      nick === undefined ||
      email === undefined ||
      !password === undefined ||
      !city === undefined
    ) {
      return { status: 'NOT OK' };
    }

    const user = await this.userRepository.findOne({
      where: [{ email }, { nick }],
    });

    if (user) {
      return { status: 'NOT OK', message: 'user exists' };
    }

    await bcrypt
      .genSalt(this.saltRounds)
      .then((salt) => {
        console.log('Salt: ', salt);
        return bcrypt.hash(password, salt);
      })
      .then((hash) => {
        this.hash = hash;
      })
      .catch((err) => {
        this.hash = null;
        console.error(err.message);
      });

    try {
      const imageName = uuidv4();
      const imagePath = UserController.config.APP_HOST +
        ':' + UserController.config.APP_PORT + '/images/' + imageName;

      
      if(typeof profileImage!=='undefined') {
        saveBase64ToFile(profileImage, UserController.uploadsPath + imageName);
      }
 

      const newUser = AppDataSource.manager.create(User, {
        nick,
        email,
        photos: { profile: imagePath, rest: [] },
        state: 'non-configured',
        hash: this.hash,
        city_id: city.id,
      });
      return { ...this.userRepository.save(newUser), status: 'OK' };
    } catch (e) {
      console.error(e);
    }

  }

  async login(request: Request, response: Response) {
    const { login, password } = request.body;
    let uuid;
    const resp: any = {};
    const user = await this.userRepository
      .createQueryBuilder('user')
      .where('user.nick = :login', { login })
      .orWhere('user.email = :login', { login })
      .getOne();

    if (!user) {
      return { status: 'NOT OK', message: 'user not exist' };
    }

    const bcryptResult = await bcrypt.compare(
      password,
      user.hash,
      async function (err, result) {
        if (err) {
          console.error('Error');
          console.error(err);
          resp.status = 'NOT OK';
          resp.message = err;
          response.send(resp);
        }

        if (result) {
          uuid = uuidv4();
          for(const uuid in UserController.users) {
            if (UserController.users[uuid].nick===user.nick) {
              delete UserController.users[uuid];
              break;
            }
          }
          UserController.users[uuid] = user;
          resp.status = 'OK';
          resp.uuid = uuid;
          user.loggedIn = true;
          AppDataSource.manager.save(user);
          resp.user = user;
          const loggedFriends = await AppDataSource.manager.query(`
            SELECT user0.id, user0.nick from user AS user0
            INNER JOIN user_user ON userFromId = user0.id
            INNER JOIN user AS user1 ON userToId=user1.id
            WHERE user1.id=? AND status!='blocked' AND status!='blocks'
            UNION
            SELECT user0.id, user0.nick from user AS user0
            INNER JOIN user_user ON userToId = user0.id
            INNER JOIN user AS user1 ON userFromId=user1.id
            WHERE user1.id=? AND status!='blocked' AND status!='blocks'
            `, [UserController.users[uuid].id, UserController.users[uuid].id])
          loggedFriends.forEach(friend => {
            for (let el in UserController.users) {
              if (UserController.users[el].id === friend.id && UserController.users[el].id !== user.id) {
                UserController.users[el].socket.send(JSON.stringify({ loggedIn: user.nick }));
              }
            }
          });

        } else {
          resp.status = 'NOT OK';
        }
        response.send(resp);
      }
    );
    console.log(bcryptResult);
  }

  async authorize(request: Request, response: Response) {
    const { password, uuid } = request.body;

    const id = UserController.users[uuid]?.id;
    console.log(id);

    const user = await this.userRepository.findOne({ where: { id } });

    if (!user) {
      return { status: 'NOT OK' };
    }
    const resp = { status: null, message: null };
    const bcryptResult = await bcrypt.compare(
      password,
      user.hash,
      function (err, result) {
        if (err) {
          console.error('Error');
          console.error(err);
          resp.status = 'NOT OK';
          resp.message = err;
          return;
        }
        console.log('result');
        console.log(result);
        if (result) {
          if(!UserController.users[uuid]) return {status: 'NOT OK'}
          UserController.users[uuid].authorization = true;
          resp.status = 'OK';

          setTimeout(() => {
            UserController.users[uuid] &&
              (UserController.users[uuid].authorization = false);
          }, 60000);
        }
        response.json(resp);
      }
    );
  }

  async changePassword(request: Request, response: Response) {
    const { uuid, password } = request.body;
    const id = UserController.users[uuid].id;
    const user = await this.userRepository.findOne({
      where: { id },
    });

    if (!user) {
      return { status: 'NOT OK', message: 'user does not exist' };
    }

    await bcrypt
      .genSalt(this.saltRounds)
      .then((salt) => {
        console.log('Salt: ', salt);
        return bcrypt.hash(password, salt);
      })
      .then(async (hash) => {
        user.hash = hash;
        await this.userRepository.save(user);
        response.json({ ...user, status: 'OK' });
      })
      .catch((err) => {
        this.hash = null;
        console.error(err.message);
      });
  }

  async change(request: Request, response: Response) {
    const { uuid, nick, city, profileImage, restOfImages } = request.body;
    const id = UserController.users[uuid].id;
    const user = await this.userRepository.findOne({
      where: { id },
    });

    if (!user) {
      return { status: 'NOT OK', message: 'user does not exist' };
    }

    user.nick = nick;
    user.city_id = city?.id || user.city_id;
    // TO DO base64 to file

    if(profileImage.match('localhost')) {
      user.photos.profile=profileImage;
    } else {
    let imageName = uuidv4();
    let imagePath = UserController.config.APP_HOST +
      ':' + UserController.config.APP_PORT + '/images/' + imageName;

    saveBase64ToFile(profileImage, UserController.uploadsPath + imageName);
    user.photos.profile = imagePath;
    }
    user.photos.rest = [];
    for (let i = 0, max = restOfImages.length; i < max; i++) {
      
      if(restOfImages[i].match('localhost')) {
        user.photos.rest.push(restOfImages[i])
      } else {
      let imageName = uuidv4();
      let imagePath = UserController.config.APP_HOST +
        ':' + UserController.config.APP_PORT + '/images/' + imageName;
      saveBase64ToFile(restOfImages[i], UserController.uploadsPath + imageName);
      user.photos.rest.push(imagePath);
      }
    }


    //    user.photos.profile = profileImage;

    //user.photos.rest = restOfImages;
    await this.userRepository.save(user);
    delete user['hash'];
    response.json({ ...user, status: 'OK' });
  }

  async logout(request: Request, response: Response) {
    const { uuid } = request.body;
    if (!UserController.users[uuid]) {
      return;
    }
    const { id } = UserController.users[uuid];
    const user = await this.userRepository.findOne({ where: { id } });
    user.loggedIn = false;
    AppDataSource.manager.save(user);
    delete UserController.users[uuid];
    console.log(UserController.users);
    const loggedFriends = await AppDataSource.manager.query(`
    SELECT user0.id, user0.nick from user AS user0
    INNER JOIN user_user ON userFromId = user0.id
    INNER JOIN user AS user1 ON userToId=user1.id
    WHERE user1.id=? AND status!='blocked' AND status!='blocks'
    UNION
    SELECT user0.id, user0.nick from user AS user0
    INNER JOIN user_user ON userToId = user0.id
    INNER JOIN user AS user1 ON userFromId=user1.id
    WHERE user1.id=? AND status!='blocked' AND status!='blocks'
    `, [id, id]);
    loggedFriends.forEach(friend => {
      for (let el in UserController.users) {
        if (UserController.users[el].id === friend.id && UserController.users[el].id !== user.id) {
          UserController.users[el].socket.send(JSON.stringify({ loggedOut: user.nick }));
        }
      }
    });
    return { status: 'OK' };
  }

  async setConfiguration(request: Request, response: Response) {
    const { uuid, age, ageRange, sex, height, restPhotos, about } = request.body;
    
     const userByUuid = UserController.users[uuid];
    const { id } = userByUuid;
    
    let user = await this.userRepository.findOne({
      where: {
        id,
      },
    });
    if (!user) {
      return { status: 'NOT OK' };
    }
    await this.userRepository.update({
      id,
    }, {
      age, ageRange, height, sex: sex === 'men', state: 'configured', about
    });
    userByUuid.age = age;
    userByUuid.ageRange = ageRange;
    userByUuid.sex = sex==='men';
    return { status: 'OK' };
  }

  async settings(request: Request, response: Response) {
    const { uuid, distance, ageRange } = request.body;
    if (!UserController.users[uuid]) {
      return { status: 'NOT OK' };
    };

    UserController.users[uuid].distance = distance;
    UserController.users[uuid].ageRange = ageRange;
    const { id } = UserController.users[uuid];
    // if(typeof distance !=='number' || !Array.isArray(ageRange)) {
    //   return { status: 'NOT OK', message: 'Wrong data format'}
    // }

    
    return await this.userRepository.update({ id }, { distance, ageRange });
  }

  async wave(request: Request, response: Response) {
    const { uuid, nick } = request.body;

    const userFrom = await this.userRepository.findOne({
      where: { id: UserController.users[uuid]?.id },
    });

    const userTo = await this.userRepository.findOne({ where: { nick } });
    if (!userFrom || !userTo) {
      return { status: 'NOT OK' };
    }

    const userUser = await this.userUserRepository
      .createQueryBuilder('userUser')
      .innerJoin('userUser.userFrom', 'userFrom')
      .innerJoin('userUser.userTo', 'userTo')
      .where(`userFromId=${userFrom.id} AND userToId=${userTo.id}`)
      .getMany();

    console.log('userUsers');
    console.log(userUser);
    if (userUser.length === 0) {
      console.log('users length 0')
      let newUser = { status: 'wave', userFrom, userTo };
      this.userUserRepository.save(newUser);
      return newUser;
    } else {
      if (userUser[0].status === 'banned') {
        return { info: 'BANNED' };
      };

      if (userUser[0].status === 'friend') {
        return { info: 'SENDED' };
      };
    };


  }

  async getFriends(request: Request, response: Response) {
    const { uuid } = request.body;
    if (typeof UserController.users[uuid] === 'undefined') {
      return {};
    }
    const { id, nick } = UserController.users[uuid];
    const userUsers: {userNick: string, user0Nick: string, blocked: boolean, blocks: boolean,loggedIn?: boolean}[] = (await AppDataSource.manager.query(
      `SELECT user0.id, user0.nick AS user0Nick,user.nick AS userNick,
        user0.loggedIn AS userLoggedIn0, user.loggedIn AS userLoggedIn,
        user_user.userToId AS userToId, user_user.userFromId AS userFromId,
        user_user.status
        FROM user_user
        LEFT JOIN user ON user_user.userFromId=user.id
        LEFT JOIN user AS user0 ON user_user.userToId=user0.id
      WHERE (user_user.userFromId=? OR user_user.userToId=?) AND
        user_user.handshake IS NOT NULL;`,
      [id, id]
    )).map(el => {
      if (el.userLoggedIn0 && el.userLoggedIn) {
        el.loggedIn = true;
      } else {
        el.userLoggedIn = false;
      }

      if (el.status === 'blocked' && el.userToId === id) {
        return { ...el, blocked: true };
      }
      else if (el.status === 'blocked' && el.userFromId === id) {
        return { ...el, blocks: true }
      }
      else {
        return el;
      }
    });


    const preselected = userUsers.map((el) => {
      const person:{nick:string, nrOfMessages: number, blocked?: boolean, blocks?:boolean,
         loggedIn?: boolean} =
        el.userNick === nick
          ? { nick: el.user0Nick, nrOfMessages: 0 }
          : { nick: el.userNick, nrOfMessages: 0 };
      person.blocked = el.blocked;
      person.blocks = el.blocks;
      person.loggedIn = el.loggedIn;
      return person;
    })

    const selected = [];
    const existing = new Set();
    for(let i=0, max=userUsers.length; i<max; i++) {
      if(!existing.has(preselected[i].nick)) {
        existing.add(preselected[i].nick);
        selected.push(preselected[i]);
      }
    }


      return selected;

  }

  async getFriend(request: Request, response: Response) {
    const { uuid, nick } = request.body;
    if (!UserController.users[uuid]) {
      return { status: 'NOT OK', message: 'User does not exist' }
    }
    const user = await this.userRepository.findOne({ where: { nick } });
    delete user['hash'];
    return user;
  }

  async getCloseFriends(request: Request, response: Response) {
    const { uuid } = request.query;
    if (!UserController.users[uuid]) {
      return [];
    };
    const user = UserController.users[uuid];
    const id = user.id;
    const city = await this.cityRepository.findOne({
      where: { id: user.city_id }
    }
    );

    const closeFriendsSql =
      `SELECT nick, city.id, city.name, city.latitude, city.longitude,
      user.photos, user.age, user.loggedIn
      FROM user INNER JOIN city ON user.city_id = city.id WHERE
      6371 * acos(cos(radians(?)) * cos(radians(city.latitude)) * cos(radians(city.longitude) - radians(?)) + sin(radians(?)) * sin(radians(city.latitude)))<=?
      AND
      user.sex!=?
      AND age BETWEEN ? AND ?
      AND NOT EXISTS (
        SELECT 1 FROM user_user
          WHERE ((userFromId=user.id AND userToId=?) 
                OR (userToId=user.id AND userFromId=?)) AND
                handshake = 'complete'
      )
      ;`;
    const ageRange: [number, number] = user.ageRange ?
      // [parseInt(user.ageRange?.split(',')[0].split('[')[1]),
      // parseInt(user.ageRange?.split(',')[1].split(']')[0])
      // ] : [0, 120];
      user.ageRange : [0, 120];


    const closeFriends = await AppDataSource.manager.query(closeFriendsSql,
      [city.latitude,
      city.longitude,
      city.latitude,
      user.distance !== null ? user.distance : 1000 * 1000,
      user.sex,
      ageRange[0],
      ageRange[1],
        id, id
      ]);
    return closeFriends;
  }

  async getCloseFriendsSorted(request: Request, response: Response) {
    const { uuid } = request.query;
    if (!UserController.users[uuid]) {
      return [];
    };
    const user = UserController.users[uuid];
    const id = user.id;
    const city = await this.cityRepository.findOne({
      where: { id: user.city_id }
    }
    );

    const closeFriendsSql =
      `SELECT nick, city.id, city.name, city.latitude, city.longitude,
      6371 * acos(cos(radians(?)) * cos(radians(city.latitude)) * cos(radians(city.longitude) - radians(?)) + sin(radians(?)) * sin(radians(city.latitude))) AS distance_km,
      user.photos, user.age, user.loggedIn
      FROM user INNER JOIN city ON user.city_id = city.id WHERE
      user.sex!=?
      AND age BETWEEN ? AND ?
      AND NOT EXISTS (
        SELECT 1 FROM user_user
          WHERE ((userFromId=user.id AND userToId=?) 
                OR (userToId=user.id AND userFromId=?)) AND
                handshake = 'complete'
      ) ORDER BY distance_km
      ;`;
    const ageRange: [number, number] = user.ageRange ?
      user.ageRange : [0, 120];


    const closeFriends = await AppDataSource.manager.query(closeFriendsSql,
      [city.latitude,
      city.longitude,
      city.latitude,
      user.sex,
      ageRange[0],
      ageRange[1],
        id, id
      ]);
    return closeFriends;
  }



  // !!!dane użytkownika, w produkcji wywalić!!!
  async getUserByNick(request: Request, response: Response) {
    const { nick } = request.query;
    const user = Object.keys(UserController.users).filter(uuid => {
      UserController.users[uuid].nick === nick;
    });
    return user;
  }

  async getMessages(request: Request, response: Response) {
    const { uuid, friendNick } = request.query;
    if (typeof UserController.users[uuid] === 'undefined') {
      return {};
    };
    const { id } = UserController.users[uuid];
    const messagesUser = await AppDataSource.manager.query(
      `SELECT * FROM user_message_user LEFT JOIN user ON user_message_user.userFromId=user.id
        LEFT JOIN user AS user0 ON user_message_user.userToId=user0.id
        LEFT JOIN message ON user_message_user.messageId = message.id
      WHERE (user.id=? AND user0.nick=?) OR (user.nick=? AND user0.id=?)
      ORDER BY message.datetime`,
      [id, friendNick, friendNick, id]
    );
    console.log('messagesUser')


    console.log(messagesUser)
    return messagesUser.map(el => { return { ...el, hash: undefined } });
  }

  async blockUser(request: Request, response: Response) {
    const { uuid, nick } = request.body;
    console.log(UserController.users[uuid].id);
    const blockedUser = await this.userRepository.findOne({ where: { nick } });
    console.log(blockedUser);
    for (let user in UserController.users) {
      if (UserController.users[user].nick === nick) {
        UserController.users[user].socket.send(JSON.stringify({ blocked: { nick: UserController.users[uuid].nick } }));
        break;
      }
    }
    let resp = await AppDataSource.manager.query(`
      UPDATE user_user SET status='blocks'
      WHERE (userFromId=? AND userToId=?)
    `, [UserController.users[uuid].id, blockedUser.id]);
    if(resp.affectedRows===0) {
      resp = await AppDataSource.manager.query(`
        UPDATE user_user SET status='blocked'
        WHERE (userFromId=? AND userToId=?)
      `, [blockedUser.id, UserController.users[uuid].id ]);
    }
    return resp;
  }

  async unBlockUser(request: Request, response: Response) {
    const { uuid, nick } = request.body;
    
    if(!UserController.users[uuid]) {
      return {status: 'NOT OK', message: 'You are not logged in'}
    }
    const user_user0 = await AppDataSource.manager.query(
      `SELECT * FROM user_user
      INNER JOIN user AS user0 ON user0.id = user_user.userFromId
      INNER JOIN user AS user1 ON user1.id = user_user.userToId
      WHERE user0.nick = ? AND user1.nick = ?
      `,[UserController.users[uuid].nick,nick]
    )
    if(user_user0[0] && user_user0[0].status==='blocked') {
      return {status: 'NOT OK'}
    }

    const user_user1 = await AppDataSource.manager.query(
      `SELECT * FROM user_user
      INNER JOIN user AS user0 ON user0.id = user_user.userFromId
      INNER JOIN user AS user1 ON user1.id = user_user.userToId
      WHERE user0.nick = ? AND user1.nick = ?
      `,[nick,UserController.users[uuid].nick]
    )
    if(user_user1[0] && user_user1[0].status==='blocks') {
      return {status: 'NOT OK'}
    }



    const blockedUser = await this.userRepository.findOne({ where: { nick } });
    for (let user in UserController.users) {
      if (UserController.users[user].nick === nick) {
        UserController.users[user].socket.send(JSON.stringify({ unblocked: { nick: UserController.users[uuid].nick } }));
        break;
      }
    }

    

    let resp = await AppDataSource.manager.query(`
      UPDATE user_user SET status='wave'
      WHERE (userFromId=? AND userToId=?)
      `, [UserController.users[uuid].id, blockedUser.id]);

      if(resp.affectedRows === 0) {
        resp = await AppDataSource.manager.query(`
          UPDATE user_user SET status='wave'
          WHERE (userFromId=? AND userToId=?)
          `, [blockedUser.id, UserController.users[uuid].id]);
      }
      return resp;
  }

  async checkIfBlocksIsBlocked(request: Request, response: Response) {
    const { nick, friend } = request.query;
    const user_user0 = await AppDataSource.manager.query(`SELECT user1.nick AS nick, status FROM user_user 
                                  INNER JOIN user AS user0 ON user0.id = userFromId
                                  INNER JOIN user AS user1 ON user1.id = userToId
                                  WHERE user0.nick=? AND user1.nick=?`,
      [nick, friend]);
    
      if (user_user0[0] && user_user0[0].status === 'blocks') {
      return { blocks: true };
    }

    if (user_user0[0] && user_user0[0].status === 'blocked') {
      return { blocked: true };
    }

    const user_user1 = await AppDataSource.manager.query(`SELECT user0.nick AS nick, status FROM user_user 
                                  INNER JOIN user AS user0 ON user0.id = userFromId
                                  INNER JOIN user AS user1 ON user1.id = userToId
                                  WHERE user0.nick=? AND user1.nick=?`,
      [friend, nick]);
      
      if (user_user1[0] && user_user1[0].status === 'blocks') {
        return { blocked: true };
      }
      if (user_user1[0] && user_user1[0].status === 'blocked') {
        return { blocks: true };
      }
  
      return {};
  }

  async acceptUser(request: Request, response: Response) {
    const { uuid, nick } = request.body;
    const userFrom = await this.userRepository.findOne({
      where: {
        id: UserController.users[uuid].id
      }
    });

    const userTo = await this.userRepository.findOne({
      where: {
        nick
      }
    });

    console.log(userFrom);
    console.log(userTo);

    const userRelation = await AppDataSource.manager.query(`
      SELECT * FROM user_user
      WHERE userFromId=? AND userToId=? 
    `, [userTo.id, userFrom.id]);


    const updateResult = await AppDataSource.manager.query(`
      UPDATE user_user SET relation='accepted' 
      WHERE userFromId=? AND userToId=? 
    `, [userFrom.id, userTo.id]);

    if (updateResult.affectedRows === 0) {
      const insertResult = await AppDataSource.manager.query(`
      INSERT INTO user_user VALUES (NULL, '', 'accepted',NULL,0,?,?)
    `, [userFrom.id, userTo.id]);

    };

    return userRelation[0] || { relation: undefined };
  }

  async rejectUser(request: Request, response: Response) {
    const { uuid, nick } = request.body;
    const userFrom = await this.userRepository.findOne({
      where: {
        id: UserController.users[uuid].id
      }
    });

    const userTo = await this.userRepository.findOne({
      where: {
        nick
      }
    });

    console.log(userFrom);
    console.log(userTo);
    const updateResult = await AppDataSource.manager.query(`
      UPDATE user_user SET relation='rejected' 
      WHERE userFromId=? AND userToId=? 
    `, [userFrom.id, userTo.id]);

    if (updateResult.changedRows === 0) {
      const insertResult = await AppDataSource.manager.query(`
      INSERT INTO user_user VALUES (NULL, '', 'rejected',?,?)
    `, [userFrom.id, userTo.id]);
      return insertResult;
    } else {
      return updateResult;
    };
  }

  async getUserImage(request: Request, response: Response) {
    const { id } = request.body;
    const user = await this.userRepository.findOne({ where: { id } });
    return { image: user.photos.profile };
  }

  async getUserImages(request: Request, response: Response) {
    const { uuid } = request.body;
    const userLogged = UserController.users[uuid];
    if (!userLogged) {
      return { status: 'NOT OK', message: 'User not found' };
    }
    const user = await this.userRepository.findOne({ where: { id: userLogged.id } });
    return { image: user.photos };
  }

  async getFriendImage(request: Request, response: Response) {
    const { uuid, friendNick } = request.body;

    if (typeof UserController.users[uuid] === 'undefined') {
      return {};
    }
    const { id } = UserController.users[uuid];
    if (typeof id === 'undefined') {
      return {};
    }

    const userUsers: [] = (await AppDataSource.manager.query(
      `SELECT user.nick AS fromNick, user.photos AS fromPhotos, user0.nick AS toNick, user0.photos AS toPhotos, status
      FROM user_user 
      INNER JOIN user ON user_user.userFromId=user.id 
      INNER JOIN user AS user0 ON user_user.userToId=user0.id 
      WHERE ((user_user.userFromId=?) OR (user_user.userToId=?))
      AND (user.nick=? OR user0.nick=?);`,
      [id, id, friendNick, friendNick]
    ));
    console.log('userUsers');
    console.log(userUsers);
    return userUsers

  }
  async getByNick(request: Request, response: Response) {
    const { uuid, nick } = request.body;
    if (typeof UserController.users[uuid] === 'undefined') {
      return { status: 'NOT OK', message: 'You are not logged in' };
    }
    const { id } = UserController.users[uuid];
    const user = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      return { status: 'NOT OK', message: 'You are not logged in' };
    }
    const friend = await this.userRepository.findOne({ where: { nick } });


    const user_user = await this.userUserRepository.createQueryBuilder('user_user')
    .where('user_user.userFromId = :id AND user_user.userToId = :friendId', { id, friendId: friend.id })
    .getMany();

    if (user_user.length > 0) {
      if (user_user[0].relation == null && user_user[0].handshake == null) {
        user_user[0].visited = true;
        this.userUserRepository.save(user_user[0]);

      }
    } else {
      const userUser = new UserUser();
      userUser.userFrom = user;
      userUser.userTo = friend;
      userUser.visited = true;
      this.userUserRepository.save(userUser);
    }

    delete friend['hash']
    return friend;
  }

  async visited(request: Request, response: Response) {
    const { uuid } = request.body;
    if (!UserController.users[uuid]) {
      return [];
    }
    const { id } = UserController.users[uuid];
    // const user = await this.userUserRepository.find({where: { id }});
    const users = await AppDataSource.manager.query(`SELECT * FROM user INNER JOIN user_user
                                  ON user_user.userFromId=user.id
                                  INNER JOIN city on user.city_id=city.id
                                  WHERE user_user.userToId=?
    `, [id]);
    

    await users.forEach(user => {
      delete user.hash;
    });

    return users;
  }
}
