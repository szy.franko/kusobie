import { Request, Response } from "express";
import * as fs from "fs";
import { v4 as uuidv4 } from "uuid";
import * as path from "path";
export class ImageController {
  async toBlob(request: Request, response: Response) {
    const { imageBase64 } = request.body;
    console.log(imageBase64);
    const name = `${uuidv4()}.png`;
    const file =path.normalize(`${__dirname}/../../uploads/${name}`);
    fs.writeFileSync(file, Buffer.from(imageBase64.path.split(",")[1], 'base64'));
    return { name };
  }
}
