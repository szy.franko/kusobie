import LoggedUser from "./LoggedUser";
export default interface LoggedUsers {
    [name: string]: LoggedUser
}