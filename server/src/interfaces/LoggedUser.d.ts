export default interface LoggedUser {
    id: number
    nick: string
    photos: object
    state: string
    email: string
    hash: string
    city_id: number
}