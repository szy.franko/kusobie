export default interface Config {
  APP_HOST?: string;
  APP_PORT?: number;
  DB_TYPE?: string;
  DB_HOST?: string;
  DB_PORT?: number;
  DB_USERNAME?: string;
  DB_PASSWORD?: string;
  DB_DATABASE?: string;

}
