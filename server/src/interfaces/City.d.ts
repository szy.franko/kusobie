export default interface City {
    id: string;
    name: string;
    type:string;
    province:string;
    district:string;
    commune:string;
    latitude:number; 
    longitude:number;
}