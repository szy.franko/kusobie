import { UserController } from './controller/UserController'
import { CityController } from './controller/CityController'
import { ImageController } from './controller/ImageController'
import { MessageController } from './controller/MessageController'

interface Route {
    method: string,
    route: string,
    controller: object,
    action: string
}

export const Routes: Route[] = [

    {
        method: 'get',
        route: '/',
        controller: UserController,
        action: 'main'
    },
    {
        method: 'post',
        route: '/user/register',
        controller: UserController,
        action: 'register'
    },
    {
        method: 'get',
        route: '/user',
        controller: UserController,
        action: 'all'
    },
    // {
    //     method: 'get',
    //     route: '/user/:id',
    //     controller: UserController,
    //     action: 'one'
    // },
    {
        method: 'post',
        route: '/user',
        controller: UserController,
        action: 'save'
    }, {
        method: 'post',
        route: '/user/login',
        controller: UserController,
        action: 'login'
    },
    {
        method: 'post',
        route: '/user/authorize',
        controller: UserController,
        action: 'authorize'
    },

    {
        method: 'post',
        route: '/user/change-password',
        controller: UserController,
        action: 'changePassword'
    },
    {
        method: 'put',
        route: '/user/change',
        controller: UserController,
        action: 'change'
    },
    {
        method: 'post',
        route: '/user/logout',
        controller: UserController,
        action: 'logout'
    },
    {
        method: 'delete',
        route: '/user/:id',
        controller: UserController,
        action: 'remove'
    },
    {
        method: 'put',
        route: '/user/configuration',
        controller: UserController,
        action: 'setConfiguration'
    },
    {
        method: 'put',
        route: '/user/settings',
        controller: UserController,
        action: 'settings'
    },
    {
        method: 'post',
        route: '/user/messages',
        controller: UserController,
        action: 'getMessages'
    },
    {
        method: 'post',
        route: '/user/friends',
        controller: UserController,
        action: 'getFriends'
    },
    {
        method: 'post',
        route: '/user/friend',
        controller: UserController,
        action: 'getFriend'
    },
    {
        method: 'get',
        route: '/user/close-friends',
        controller: UserController,
        action: 'getCloseFriends'
    },
    {
        method: 'get',
        route: '/user/close-friends-sorted',
        controller: UserController,
        action: 'getCloseFriendsSorted'
    },
    {
        method: 'get',
        route: '/user/nick',
        controller: UserController,
        action: 'getUserByNick'
    },
    {
        method: 'post',
        route: '/user/wave',
        controller: UserController,
        action: 'wave'
    },
    {
        method: 'get',
        route: '/user/messages',
        controller: UserController,
        action: 'getMessages'
    },
    {
        method: 'put',
        route: '/user/block',
        controller: UserController,
        action: 'blockUser'
    },
    {
        method: 'put',
        route: '/user/unblock',
        controller: UserController,
        action: 'unBlockUser'
    },
    {
        method: 'get',
        route: '/users/check-if-blocks-is-blocked/',
        controller: UserController,
        action: 'checkIfBlocksIsBlocked'
    },
    {
        method: 'put',
        route: '/user/accept',
        controller: UserController,
        action: 'acceptUser'
    },

    {
        method: 'put',
        route: '/user/reject',
        controller: UserController,
        action: 'rejectUser'
    },

    {
        method: 'post',
        route: '/user/image',
        controller: UserController,
        action: 'getUserImage'
    },
    {
        method: 'post',
        route: '/user/images',
        controller: UserController,
        action: 'getUserImages'
    },
    {
        method: 'get',
        route: '/user/get-by-email',
        controller: UserController,
        action: 'checkEmail'
    },
    {
        method: 'get',
        route: '/user/get-by-nick',
        controller: UserController,
        action: 'checkNick'
    },

    {
        method: 'post',
        route: '/user/get-friend-image',
        controller: UserController,
        action: 'getFriendImage'
    },
    {
        method: 'post',
        route: '/user/get-by-nick/',
        controller: UserController,
        action: 'getByNick'
    },
    {
        method: 'post',
        route: '/user/visited',
        controller: UserController,
        action: 'visited'
    },
    {
        method: 'put',
        route: '/message/read',
        controller: MessageController,
        action: 'messageRead'
    },
    {
        method: 'post',
        route: '/message/unread',
        controller: MessageController,
        action: 'messageUnread'
    },
    {
        method: 'post',
        route: '/city',
        controller: CityController,
        action: 'get'
    },
    {
        method: 'post',
        route: '/city/by-id',
        controller: CityController,
        action: 'getById'
    },
    {
        method: 'get',
        route: '/city/generate',
        controller: CityController,
        action: 'generate'
    },
    {
        method: 'post',
        route: '/image/to-blob',
        controller: ImageController,
        action: 'toBlob'
    }
]