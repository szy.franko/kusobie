import * as fs from 'fs';
import Config from '../interfaces/Config';
export default class AppConfig {

    private static config:Config;
    
    constructor() {
        if(AppConfig.config) {
            return;
        }
        const rawConfig = fs.readFileSync(__dirname+'/../../../app/.env').toString();
        const rawConfigArray = rawConfig.split('\n');
        AppConfig.config={};
        
        let line;
        for(let i=0, max = rawConfigArray.length; i<max;i++) {
            if(rawConfigArray[i].trim()[0]==='0') {
                continue;
            }
            line = rawConfigArray[i].split('=');
            if(line.length<2) {
                continue;
            }
            AppConfig.config[line[0]] = line[1];
        }

        console.log(AppConfig.config)
    }
    get config():Config {
        return AppConfig.getConfig()
    }
    static getConfig():Config {
        return AppConfig.config;
    }

}