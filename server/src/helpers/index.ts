import * as fs from "fs";


export function filterObjectFields(originalObject: object,
     fieldsToRemove: string[]): object{
    return Object.keys(originalObject).reduce((filteredObject, key) => {
      if (!fieldsToRemove.includes(key)) {
        filteredObject[key] = originalObject[key];
      }
      return filteredObject;
    }, {});
  }

  export function saveBase64ToFile(base64: string, filePath: string){
    const base64Data = base64.replace(/^data:image\/\w+;base64,/, '');
    const binaryData = Buffer.from(base64Data, 'base64');
    
    fs.writeFile(filePath, binaryData, 'binary', (err) => {
      if (err) {
          console.error('Error:', err);
      } else {
          console.log('File created successfully');
      }
  });
  }
  