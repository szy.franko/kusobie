import 'reflect-metadata'
import * as fs from 'fs';
import { DataSource, ServerCapabilities } from 'typeorm'
import { User } from './entities/User.entity'
import { Message } from './entities/Message.entity'
import { UserMessageUser } from './entities/UserMessageUser.entity'
import { City } from './entities/City.entity'
import { UserCity } from './entities/UserCity.entity';
import { UserUser } from './entities/UserUser.entity';
import AppConfig from './classes/AppConfig';

const config = new AppConfig().config;

export const AppDataSource = new DataSource({
    type: 'mysql',
    host: config['DB_HOST'],
    port: 3306,
    username: config['DB_USERNAME'],
    password: config['DB_PASSWORD'],
    database: config['DB_DATABASE'],
    synchronize: true,
    
    logging: false,
    entities: [User, Message, UserMessageUser, City, UserCity, UserUser],
    migrations: [],
    subscribers: []
})

