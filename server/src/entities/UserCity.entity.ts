import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { User } from './User.entity';
@Entity({name: 'user_enity', schema: 'public'})
export class UserCity {
    @PrimaryGeneratedColumn()
    id: number
    @ManyToOne(type=>User, user=>user.userCities) user: User;
}