import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, JoinColumn } from 'typeorm';
import { User } from './User.entity';
import { Message } from './Message.entity';
import { type } from 'os';
@Entity({name: 'user_message_user', schema: 'public'})
export class UserMessageUser {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    status:boolean



    @ManyToOne(type => User, userFrom => userFrom.userMessageUsersFrom) userFrom:User;
   
    @ManyToOne(type => User, userTo => userTo.userMessageUsersTo) userTo:User;

    @OneToOne(type=>Message, {cascade: true})
    @JoinColumn()
    message: Message


}
