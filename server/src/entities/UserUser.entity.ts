import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne, JoinColumn } from "typeorm";
import { User } from './User.entity';

@Entity({name: 'user_user', schema: 'public'})
export class UserUser {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    status: string
    
    @Column()
    relation: string
    
    @Column({nullable: true})
    handshake:string
    
    @Column({nullable: true})
    visited: boolean
    
    @ManyToOne(() => User, (userFrom) => userFrom.userUser) userFrom: User
    
    @ManyToOne(() => User, (userTo) => userTo.userUser) userTo: User
    user: User

    }
