import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({name: 'message', schema: 'public'})
export class Message {
  @PrimaryGeneratedColumn()
  id: number;

  // @Column()
  // idUserMessageUser: number;

  @Column('text')
  content: string;

  @Column('datetime')
  datetime: Date;
}
