import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity({name: 'city', schema: 'public'})
export class City {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  type: string;

  @Column()
  province: string;

  @Column()
  district: string;

  @Column()
  commune: string;

  @Column({name: 'latitude', type: 'decimal', scale: 5, precision: 10})
  latitude: number;
  
  @Column({name: 'longitude', type: 'decimal', scale: 5, precision: 10})
  longitude: number;
}

// "Id":"1",
// "Name":"Abramowice Kościelne",
// "Type":"village",
// "Province":"lubelskie",
// "District":"lubelski",
// "Commune":"Głusk-gmina wiejska",
// "Latitude":51.1914,
// "Longitude":22.6294
// }
