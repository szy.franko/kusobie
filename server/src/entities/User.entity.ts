import { Entity, PrimaryGeneratedColumn, Column, OneToMany, getTreeRepository } from 'typeorm';
import { UserMessageUser } from './UserMessageUser.entity';
import { type } from 'os';
import { UserCity } from './UserCity.entity';
import { UserUser } from './UserUser.entity';
@Entity({name: 'user', schema: 'public'})
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  nick: string;

  @Column('json')
  photos: { profile: string; rest: string[] };

  @Column()
  state: string;

  @Column()
  email: string;

  @Column()
  hash: string;

  @Column()
  city_id: number;

  @Column({ nullable: true })
  distance: number;

  // @Column({
  //   nullable: true,
  //   type: 'json'
  // })
  // images: {profile: string, rest: string[]}

  @Column({
    nullable: true
  })
  age: number;

  @Column({
    nullable: true,
    type: 'json'
  })
  ageRange: { lower: number, upper: number}

  @Column({
    nullable: true
  })
  sex: boolean;

  @Column({
    nullable: true
  })
  height: number;
  
  @Column({nullable: true})
  about: string;

  @Column({nullable: false, default: false})
  loggedIn: boolean;

  @OneToMany(
    type => UserMessageUser, userMessageUserFrom => userMessageUserFrom.userFrom
    ,{ cascade: true }
  ) userMessageUsersFrom: UserMessageUser[];

  @OneToMany(
    type => UserMessageUser, userMessageUserTo => userMessageUserTo.userTo
    ,{ cascade: true }
  ) userMessageUsersTo: UserMessageUser[];

    @OneToMany(
      type=>UserCity, userCity=>userCity.user, {cascade: true}
    ) userCities: UserCity[] 
    
    @OneToMany(() => UserUser, (userUser) => userUser.userFrom)
    userUser: UserUser[]

    @OneToMany(() => UserUser, (userUser0) => userUser0.userTo)
    userUser0: UserUser[]

}





;