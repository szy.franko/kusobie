// import "reflect-metadata";
import * as http from 'http';
import * as Websocket from 'ws';
import app from './index';
import { AppDataSource } from './data-source';
import AppConfig from './classes/AppConfig';
import { User } from './entities/User.entity';
import { UserMessageUser } from './entities/UserMessageUser.entity';
import { UserUser } from './entities/UserUser.entity';
import { UserController } from './controller/UserController';
import { Message } from './entities/Message.entity';


const userRepository = AppDataSource.getRepository(User);
const userMessageUserRepository = AppDataSource.getRepository(UserMessageUser);
const userUserRepository = AppDataSource.getRepository(UserUser);
const messageRepository = AppDataSource.getRepository(Message);
const users: object = UserController.users;

console.log(users);
const server = http.createServer(app);
const wss = new Websocket.Server({ server });

const config = AppConfig.getConfig();

const serverPort = process.env.PORT || config.APP_PORT;

wss.on('connection', (connection) => {
  
  connection.on('message', async (data) => {
    const parsedData = JSON.parse(data.toString());
    console.log('message data');
    console.log(parsedData);
    
    if(Object.keys(parsedData).includes('uuid')) {
      if(!users[parsedData.uuid]) return;
      users[parsedData.uuid].socket = connection;
      return;
    }

    if (Object.keys(parsedData).includes('message')) {
 
      let nick = Object.keys(parsedData.message).find(el=>el!=='from');  
      
      let message = parsedData.message[nick];
      
      
      let userFromUuid = parsedData.message?.from;
      if(!users[userFromUuid]) return;
      const userFrom = await userRepository.findOne({
        where: { nick: users[userFromUuid].nick },
         relations: { 
          userMessageUsersFrom: true 
        }
      });
      
      const userTo = await userRepository.findOne({
        where: { nick },
        relations: {
          userMessageUsersTo: true 
        }
      });


      if (!userTo) {
        return;
      };
    


    const userMessageUser = new UserMessageUser();
    const messageObj = new Message();


    userMessageUser.status = false;

    userFrom.userMessageUsersFrom.push(userMessageUser);
    userTo.userMessageUsersTo.push(userMessageUser);
    messageObj.content = JSON.parse(message).content;
    messageObj.datetime = (new Date(JSON.parse(message).datetime));//.toISOString().slice(0, 19).replace('T', ' ');
    userMessageUser.message = messageObj;  
    await userRepository.save(userFrom);
    await userRepository.save(userTo);
    
    messageRepository.save(messageObj);
    await userMessageUserRepository.save(userMessageUser);


      
    const userUser = await AppDataSource.manager.query(`
    SELECT * from user_user WHERE
    userFromId=? AND userToId=?`,[userFrom.id,userTo.id]  
    );

    if(userUser[0] && userUser[0].handshake === null){
      userUser[0].handshake = 'init';
      userUserRepository.save(userUser[0]);	
    }
    
    const userUserReverse = await AppDataSource.manager.query(`
    SELECT * from user_user WHERE
    userFromId=? AND userToId=?`,[userTo.id,userFrom.id]  
    );

    if(userUserReverse[0] && userUserReverse[0].handshake === 'init'){
      userUserReverse[0].handshake = 'complete';
      userUserRepository.save(userUserReverse[0]);	
    }
    
    const receiverUserUuid = Object.keys(users).find(el=>{
        return users[el].nick ===nick;
      });
      if(receiverUserUuid) {
      
     

      if(userUser[0]?.status==='blocked') {
        return;
      }
      users[receiverUserUuid].socket.send(JSON.stringify({message:{...messageObj,...userFrom}}));
    };
    }
  });

  wss.on('error', console.error);
  wss.on('close',()=>{
    console.log('close')
  });
});

server.listen(serverPort, () => {
  console.log(`Websocket server started on port ` + serverPort);
});
